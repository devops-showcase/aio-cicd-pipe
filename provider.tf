//
// Terraform provider requirements and configuration
//

terraform {
  required_version = "~> 1.0"

  required_providers {
    opentelekomcloud = {
      source = "opentelekomcloud/opentelekomcloud"
      version = "~> 1.31.4"
    }
  }
}

provider "opentelekomcloud" {
   domain_name = var.domain_name
   tenant_name = var.tenant_name
   auth_url    = "https://iam.eu-nl.otc.t-systems.com/v3"
   user_name   = var.username
   password    = var.password
}
