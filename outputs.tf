output "fip_notify" {
  value = "Please use the following public IP to connect to your load balancer:"
}

output "lb_floating_ip" {
  value = opentelekomcloud_networking_floatingip_v2.csi_fipr.address
}
