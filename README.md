# CI/CD example pipeline for Gitlab

In this example we are creating a complete CI/CD pipeline for provisioning a very simple load balancer application using Terraform on an Open Telekom Cloud (OTC) Elastic Cloud Server (ECS).

The pipeline runs on shared GitLab.com runners. Sensitive Terraform variables are stored in the CI/CD variables, while state for each environment is stored in the Gitlab.com http backend. Docker containers are stored in the Gitlab.com container registry

## How to use

The use of this repository for learning and experimentation is encouraged. 

Some changes have to be made to the repo for the pipeline to function.
The following variables should be added under Settings / CI/CD / Variables to authenticate with OTC:

* TF_VAR_USERNAME
* TF_VAR_PASSWORD
* TF_VAR_DOMAIN_NAME

It is recommended to mask these variables so they don't show up in the job logs.

Lines 11 and 12 should include the path to the container registry of your repository.

In the current configuration the containers are only built when a scheduled pipeline is ran. Either remove this rule from the pipeline,
or schedule a run. It is recommended to run the scheduled pipeline manually for the first time to make sure a container is built.