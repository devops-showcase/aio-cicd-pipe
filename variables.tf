### OpenStack Credentials
variable "username" {
  type        = string
  description = "OTC username"
}

variable "password" {
  type        = string
  description = "OTC password"
}

variable "domain_name" {
  type        = string
  description = "OTC domain name"
}

variable "tenant_name" {
  type        = string
  description = "OTC tenant name"
  default = "eu-nl"
}

variable "endpoint" {
  type        = string
  description = "OTC endpoint name"
  default = "https://iam.eu-nl.otc.t-systems.com:443/v3"
}

variable "avail_zone" {
  type        = string
  description = "OTC availability zone"
}

### Project Settings
variable "subnet_id" {
  default = "9b601215-b28f-4297-9c44-9ee228b2212b"
}

variable "vpc_id" {
  default = "b20f6ae0-c0ee-44f9-9e3b-1c85197e4e2c"
}

variable "image_id" {
  type        = string
  description = "OTC ECS image ID"
}

variable "keypair" {
  type        = string
  description = "OTC key pair"
}

variable "secgroups" {
  type        = string
  description = "OTC security group"
}