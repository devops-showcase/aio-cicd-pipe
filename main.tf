resource "opentelekomcloud_ecs_instance_v1" "csi_lb" {
  name              = "csotiistvan-lb_${var.avail_zone}"
  vpc_id            = var.vpc_id 
  flavor            = "s3.medium.1"
  availability_zone = var.avail_zone
  key_name          = var.keypair
  image_id          = var.image_id
  security_groups   = [var.secgroups]
  system_disk_size  = 12
  system_disk_type  = "SAS"
  nics {
    network_id      = var.subnet_id
    }

  tags = {
    owner           = "Csoti_Istvan"
    provisioner     = "Managed_by_Terraform"
    }
  # For cloud-init provisioning  
  user_data         = file("cloudinit.yaml")
}

resource "opentelekomcloud_networking_floatingip_v2" "csi_fipr" {
}

resource "opentelekomcloud_networking_floatingip_associate_v2" "csi_lb_fipr" {
  floating_ip       = opentelekomcloud_networking_floatingip_v2.csi_fipr.address
  port_id           = opentelekomcloud_ecs_instance_v1.csi_lb.nics.0.port_id
}
